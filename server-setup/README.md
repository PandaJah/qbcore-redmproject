# Server setup RedM

1. Download the latest build from http://runtime.fivem.net/artifacts/fivem/build_server_windows/master/ (same as fivem)
2. Extract in a designated folder where your server will be run from
3. Download the "resources" folder and the server.cfg from this git and put it in the same place where you can see "citizen" folder (same as fivem)
4. Download all the scripts from this git and put them inside the resources folder in other folders named to your liking (same as fivem)

quick dirty explanation for the others working on the project, not official documentation