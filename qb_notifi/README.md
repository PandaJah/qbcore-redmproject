# script qb_notifi 

# EXMAPLE USAGE:

https://imgur.com/a/zx3jWt4
TriggerEvent('qb_notifi:NotifyLeft', "first text", "second text", "generic_textures", "tick", 4000)

https://imgur.com/a/28MmoRS
TriggerEvent('qb_notifi:Tip', "your text", 4000)

https://imgur.com/a/8sac0gx
TriggerEvent('qb_notifi:NotifyTop', "your text", "TOWN_ARMADILLO", 4000)

https://imgur.com/a/0f04o7D
TriggerEvent('qb_notifi:ShowSimpleRightText', "your text",  4000)

https://imgur.com/a/4DxC9wE
TriggerEvent('qb_notifi:ShowObjective', "your text", 4000)

https://imgur.com/a/LRhyOoI
TriggerEvent('qb_notifi:ShowTopNotification', "your text", "your text", 4000)

https://imgur.com/a/wYheY78
TriggerEvent('qb_notifi:ShowAdvancedRightNotification', "your text", "generic_textures" , "tick" , "COLOR_PURE_WHITE", 4000)

https://imgur.com/a/JMNPlgo
TriggerEvent('qb_notifi:ShowBasicTopNotification', "your text", 4000)

https://imgur.com/a/DSCcwry
TriggerEvent('qb_notifi:ShowSimpleCenterText', "your text", 4000)
