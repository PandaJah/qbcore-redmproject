fx_version 'cerulean'
game 'rdr3'
rdr3_warning 'I acknowledge that this is a prerelease build of RedM, and I am aware my resources *will* become incompatible once RedM ships.'

description 'QB-notifys'
version '1.0.0'


client_scripts {
	'notification/cl_notification.js',
	'notification/cl_notification.lua'
}

server_scripts {
	'notification/cl_notification.js'
}



files {
	'notification/cl_notification.js'
}
